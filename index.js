import logging from "external:@totates/logging/v1";
import EventEmitter from "external:@totates/events/v1";

const logger = logging.getLogger("@totates/indexeddb/v1");
logger.setLevel("INFO");

const WRITE_METHODS = ["add", "put", "del"];

class Store extends EventEmitter {
    constructor(db, name) {
        super();
        this.db = db; // TODO: weakref
        this.name = name;
    }

    add(item) {
        return new Promise((resolve, reject) => {
            const transaction = this.db.transaction(this.name, "readwrite");
            const store = transaction.objectStore(this.name);
            const request = store.add(item);
            request.onsuccess = e => {
                this.emit("change", [["add", e.target.result]]);
                resolve(e.target.result);
            };
            request.onerror = e => reject(e.target.error);
        });
    }

    put(item, key) {
        return new Promise((resolve, reject) => {
            const transaction = this.db.transaction(this.name, "readwrite");
            const store = transaction.objectStore(this.name);
            const request = store.put(item, key);
            request.onsuccess = e => {
                this.emit("change", [["put", e.target.result]]);
                resolve(e.target.result);
            };
            request.onerror = e => reject(e.target.error);
        });
    }


    putAll(items) {
        return new Promise((resolve, reject) => {

            const changes = [];
            const transaction = this.db.transaction(this.name, "readwrite");
            transaction.oncomplete = e => {
                this.emit("change", changes);
                resolve(true);
            };
            transaction.onabort = e => resolve(false);
            transaction.onerror = e => reject(e.target.error);

            const store = transaction.objectStore(this.name);

            const put = i => {
                if (i < items.length) {
                    const request = store.put(items[i]);
                    request.onsuccess = e => {
                        changes.push(["put", e.target.result]);
                        put(i + 1);
                    };
                    request.onerror = e => reject(e.target.error);
                }
            };
            put(0);
        });
    }

    del(item) {
        return new Promise((resolve, reject) => {
            const transaction = this.db.transaction(this.name, "readwrite");
            const store = transaction.objectStore(this.name);
            const request = store.delete(item);
            request.onsuccess = e => {
                this.emit("change", [["del", e.target.result]]);
                resolve(e.target.result);
            };
            request.onerror = e => reject(e.target.error);
        });
    }

    delAll(query) {
        return new Promise((resolve, reject) => {
            const changes = [];
            const transaction = this.db.transaction(this.name, "readwrite");
            const store = transaction.objectStore(this.name);
            const request = store.openKeyCursor();
            request.onsuccess = e => {
                const cursor = e.target.result;
                if (cursor) {
                    if (!query || query(cursor.value)) {
                        changes.push(["del", cursor.value]);
                        store.delete(cursor.primaryKey);
                    }
                    cursor.continue();
                }
                else {
                    this.emit("change", changes);
                    resolve();
                }
            };
            request.onerror = e => reject(e.target.error);
        });
    }

    get(itemName) {
        return new Promise((resolve, reject) => {
            const transaction = this.db.transaction(this.name);
            const store = transaction.objectStore(this.name);
            const request = store.get(itemName);
            request.onsuccess = e => resolve(e.target.result);
            request.onerror = e => reject(e.target.error);
        });
    }

    find(indexName, itemName) {
        return new Promise((resolve, reject) => {
            const transaction = this.db.transaction(this.name);
            const store = transaction.objectStore(this.name);
            const index = store.index(indexName);
            logger.debug("searching", itemName, "in", this.name, "by", indexName);
            const request = index.get(itemName);
            request.onsuccess = e => resolve(e.target.result);
            request.onerror = e => reject(e.target.error);
        });
    }

    findKey(indexName, itemName) {
        return new Promise((resolve, reject) => {
            const transaction = this.db.transaction(this.name);
            const store = transaction.objectStore(this.name);
            const index = store.index(indexName);
            logger.debug("searching", itemName, "in", this.name, "by", indexName);
            const request = index.getKey(itemName);
            request.onsuccess = e => resolve(e.target.result);
            request.onerror = e => reject(e.target.error);
        });
    }

    getAll() {
        return new Promise((resolve, reject) => {
            const transaction = this.db.transaction(this.name);
            const store = transaction.objectStore(this.name);
            const request = store.getAll();
            request.onsuccess = e => resolve(e.target.result);
            request.onerror = e => reject(e.target.error);
        });
    }

    getAllKeys(query) {
        return new Promise((resolve, reject) => {
            const transaction = this.db.transaction(this.name);
            const store = transaction.objectStore(this.name);
            const request = store.getAllKeys(query);
            request.onsuccess = e => resolve(e.target.result);
            request.onerror = e => reject(e.target.error);
        });
    }

    select(query) {
        /** @todo any way to convert to async generator? */
        return new Promise((resolve, reject) => {
            const transaction = this.db.transaction(this.name);
            const store = transaction.objectStore(this.name);
            const request = store.openCursor();
            const matches = [];
            request.onsuccess = e => {
                const cursor = e.target.result;
                if (cursor) {
                    if (!query || query(cursor.value)) {
                        // logger.debug('selected', cursor.value);
                        matches.push(cursor.value);
                    }
                    cursor.continue();
                }
                else {
                    // logger.debug('selection', matches);
                    resolve(matches);
                }
            };
            request.onerror = e => reject(e.target.error);
        });
    }

    selectAsObject(query) {
        /** @todo instead of returning array, return dict with store.keyPath as key */
    }

    selectFirst(query) {
        return new Promise((resolve, reject) => {
            const transaction = this.db.transaction(this.name);
            const store = transaction.objectStore(this.name);
            const request = store.openCursor();
            request.onsuccess = e => {
                const cursor = e.target.result;
                if (cursor) {
                    if (query(cursor.value))
                        resolve(cursor.value);
                    else
                        cursor.continue();
                }
                else
                    resolve(null);
            };
            request.onerror = e => reject(e.target.error);
        });
    }

    batchTransaction(mode, actions) {
        return new Promise((resolve, reject) => {

            const changes = [];

            const transaction = this.db.transaction(this.name, mode);
            transaction.oncomplete = e => {
                this.emit("change", changes);
                resolve(true);
            };
            transaction.onabort = e => resolve(false);
            transaction.onerror = e => reject(e.target.error);

            const store = transaction.objectStore(this.name);

            const doit = i => {
                if (i < actions.length) {
                    const method = actions[i].shift();
                    const request = store[method](...actions[i]);
                    request.onsuccess = e => {
                        if (WRITE_METHODS.indexOf(method) !== -1)
                            changes.push([method, ...actions[i]]);
                        doit(i + 1);
                    };
                    request.onerror = e => reject(e.target.error);
                }
            };
            doit(0);
        });
    }

    async observe(name, cb) {
        const transaction = this.db.transaction(this.name, "readonly");
        const opts = { values: true, operationTypes: ["put", "delete", "clear"] };
        const observer = new window.IDBObserver(
            changes => changes.records.get(this.name).forEach(record => {
                const key = record.key;
                if (key.lower === key.upper &&
                    key.lower === name &&
                    key.lowerOpen === false &&
                    key.upperOpen === false)
                    cb(record.type, record.value);
            })
        );

        observer.observe(this.db, transaction, opts);

        /* return function to cancel observation */
        return () => observer.unobserve(this.db);
    }
}

export default class DB extends EventEmitter {
    constructor(name, upgrade, version) {
        super();
        this.name = name;
        this.upgrade = upgrade;
        this.version = version;
        this.stores = {};
    }

    delete() {
        return new Promise((resolve, reject) => {
            const request = /* window.|self.*/indexedDB.deleteDatabase(this.name);
            request.onsuccess = e => resolve();
            request.onerror = e => reject(e.target.error);
        });
    }

    open() {
        /** @todo close existing or throw exception is already open? */
        return new Promise((resolve, reject) => {
            const request = /* window.|self.*/indexedDB.open(this.name, this.version);
            request.onerror = e => reject(e.target.error);
            request.onsuccess = e => {
                this.db = e.target.result;
                resolve();
            };
            request.onupgradeneeded = e => {
                if (!this.upgrade)
                    throw new Error("db opened without needed upgrade function");
                this.upgrade(e.target.result, e.oldVersion);
            };
        });
    }

    getStore(storeName) {
        if (!(storeName in this.stores)) {
            const store = new Store(this.db, storeName, logger);
            this.stores[storeName] = store;
        }
        return this.stores[storeName];
    }

    static transactionEnd(transaction) {
        return new Promise((resolve, reject) => {
            transaction.oncomplete = e => resolve(true);
            transaction.onabort = e => {
                /** @todo deal with quota
                 * @see https://stackoverflow.com/questions/43879950 */
                console.warn("transaction aborted", e.target.error);
                resolve(false);
            };
            transaction.onerror = e => reject(e.target.error);
        });
    }

    multiStoreBatchTransaction(actions, exec) {
        return new Promise((resolve, reject) => {

            const changes = {};
            const storeNames = Object.keys(actions);

            const onerror = e => reject(e.target.error);

            const transaction = this.db.transaction(storeNames, "readwrite");
            transaction.oncomplete = e => {
                for (const storeName in changes)
                    this.stores[storeName].emit("change", changes[storeName]);
                resolve(true);
            };
            transaction.onabort = e => resolve(false);
            transaction.onerror = onerror;

            let i = 0;
            let j = -1;

            const doit = () => {

                /* find next i and j, or return if done */
                j++;

                for (;;) {
                    if (i < storeNames.length) {
                        if (j < actions[storeNames[i]].length)
                            break;

                        i++;
                        j = 0;
                    }
                    else {
                        if (exec) {
                            try {
                                exec();
                            }
                            catch (e) {
                                transaction.abort();
                            }
                        }
                        return; /* done */
                    }
                }

                const storeName = storeNames[i];
                const action = actions[storeName][j];
                const store = transaction.objectStore(storeName);

                if (!(storeName in changes))
                    changes[storeName] = [];

                /** @todo use action[0] and action.splice(1) instead? */
                const method = action.shift();
                const request = store[method](...action);
                request.onsuccess = () => {
                    if (WRITE_METHODS.indexOf(method) !== -1)
                        changes[storeName].push([method, ...action]);
                    doit();
                };
                request.onerror = onerror;
            };

            doit();
        });
    }
}

export async function openDB(name, upgrade, version = 1) {
    const db = new DB(name, upgrade, version);
    /* db.onerror = function(event) {
        logger.error(event.target);
        window.alert('Database error: ' + event.target.wePutrrorMessage || event.target.error.name || event.target.error || event.target.errorCode);
    };*/
    await db.open();
    return db;
}
